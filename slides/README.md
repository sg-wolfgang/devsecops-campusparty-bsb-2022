---
marp: true
theme: uncover
paginate: true
backgroundColor: #fff
color: #192a56
colorSecondary: #40739e
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
---

<!-- _paginate: false -->
# <!--fit--> DevSecOps com **OpenSource**
## Análises automatizadas!
Samuel Gonçalves

---

> #### *A ciência da computação não é mais sobre computadores do que a astronomia é sobre telescópios.*

*Edsger Dijkstra*

---

![bg right:30% 80%](images/perfil.png)

#### 🇧🇷 **Samuel Gonçalves Pereira**

* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência
* *"3k++"* alunos ensinados no 🌎
* Músico, Contista, YouTuber e Podcaster

---

### Entre em contato

[https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---

<style scoped>
  p {
  font-size: 19pt;
  list-style-type: circle;
}
</style>
![bg left:40% 80%](images/qrcode.png)
### **Acesse o código fonte destes slides!**
[https://gitlab.com/sg-wolfgang/devsecops-campusparty-bsb-2022](https://gitlab.com/sg-wolfgang/devsecops-campusparty-bsb-2022)

---

### **SORTEIO NO FIM DA PALESTRA!**
![bg right:50% 80%](images/qr_sorteio4linux.png)
##### Inscreva-se e participe!
Escaneie o QRCode ou [clique aqui!](https://sorteio.4linux.com.br/cpbsb4)

---

## **DevOps**

Cultura
Colaboração
Agilidade

---

### **Fases do Ciclo de Desenvolvimento**
![bg right:50% 80%](images/phases-sldc.png)

---

#### **Ciclo de vida de desenvolvimento de software**
![w:900px](images/ciclo-devsecops.png)

---

#### **Secure Software Development Life Cycle**
![w:1000px](images/ssdlc-lifecicle.png)

É importante trazer a segurança para o projeto deste o início do processo. Esta abordagem é conhecida como **Secure Shift Left**.

---
<style scoped>
  p {
  font-size: 13pt;
}
</style>
![w:900px](images/cartoon-securitytools-3.png)
Fonte: https://blog.gitguardian.com/security-tools-shift-left/

---

## **DevSecOps**

Representa uma evolução natural e necessária na forma como as organizações de desenvolvimento abordam a segurança.

---

No passado, a segurança era 'acrescentada' ao software no final do ciclo de desenvolvimento _(quase como uma reflexão tardia)_ por uma equipe de segurança separada e testada por uma equipe de garantia de qualidade (QA) também separada.

---

### **Benefícios do DevSecOps**

* Entrega de software rápida e com boa relação custo-benefício
* Segurança aprimorada e proativa
* Correção de vulnerabilidade de segurança acelerada
* Automação compatível com o desenvolvimento moderno
* Um processo repetível e adaptativo

---
<!-- _backgroundImage: "linear-gradient(to bottom, #ECF0F1, #fff)" -->
![grayscale blur:2px bg left:40%](images/direction.jpg)
## Direcionamentos Práticos

---

### **Design Seguro**
Nesta etapa podemos usar a **Modelagem de Ameaças**. Os dois modelos principais são: STRIDE e DREAD. O **STRIDE** categoriza de acordo com ameaças (Spoofing, Tampering, Repudiation, Information Disclosure, Denial of Service e Escalation of Privilege), enquanto o **DREAD** categoriza os riscos de forma quantitativa, pontuando a severidade alta, média ou baixa. 

---
<style scoped>
  p {
  font-size: 25pt;
}
</style>

#### **Ferramentas para Modelagem de Ameaças**
Existem várias ferramentas criadas pela OWASP, como por exemplo o projeto de verificação de segurança de aplicações disponível no link:
[https://owasp.org/www-project-application-security-verification-standard/](https://owasp.org/www-project-application-security-verification-standard/)

Ou mesmo a OWASP Top 10, uma lista com as 10 principais vulnerabilidades web.

---

#### **O que proteger?**
![w:1000px](images/4cs.png)

---
<!-- _paginate: false -->

![bg center](images/DevSecOps-SAST-Pipeline.png)

---

![bg left:30%](images/DevSecOps-SAST-Pipeline.png)
Falando da camada de **CÓDIGO**, **SAST** e **DAST** são frequentemente usados ​​em conjunto porque o **SAST** não encontra erros durante o tempo de execução e o **DAST** não sinaliza erros de codificação. Nesse caso, **ambos os modelos são aplicáveis e complementares.**

---
<!-- _backgroundImage: "linear-gradient(to bottom, #ECF0F1, #fff)" -->
<!-- _paginate: false -->

## SAST
![grayscale blur:3px bg left:40%](images/sast-bg.jpg)
Acrônimo para **Teste Estático de Segurança de Aplicação**, analisa o código fonte dos sistemas. Os testes  são realizados **antes** que o sistema esteja em produção.

---

#### **Vantagens da implementação**

* Ajuda a encontrar problemas mais cedo, antes da implantação;
* Garante a conformidade com as diretrizes e padrões de codificação sem realmente executar o código;
* Como olha diretamente para o seu código, fornece informações detalhadas que sua equipe de engenharia pode usar facilmente para corrigir os problemas.

---

### **Ferramentas para SAST**

<style>
table {
    height: 50%;
    width: 80%;
    font-size: 23px;

}
th {

}
</style>

Ferramenta        | Linguagem               | Link para acesso
:---------------: |:-----------------------:|:----------------:
Brakeman          | Ruby                    | [https://brakemanscanner.org/](https://brakemanscanner.org/)
SonarQube         | 27 linguagens           | [https://www.sonarqube.org/](https://www.sonarqube.org/)
Bandit            | Python                  | [https://pypi.org/project/bandit/](https://pypi.org/project/bandit/)
Horusec           | Várias linguagens       | [https://horusec.io/](https://horusec.io/site/)
CPPCheck          | C++                     | [http://cppcheck.sourceforge.net/](http://cppcheck.sourceforge.net/)
Graudit           | Groovy                  | [https://github.com/wireghoul/graudit](https://github.com/wireghoul/graudit)
Dependency Check (SCA) | Dependências de Código  | [https://owasp.org/www-project-dependency-check/](https://owasp.org/www-project-dependency-check/)

---
<!-- _backgroundImage: "linear-gradient(to bottom, #ECF0F1, #fff)" -->
<!-- _paginate: false -->

# DAST
![grayscale blur:3px bg right:40%](images/dast-bg.jpg)
**Teste Dinâmico de Segurança de Aplicação** testa as interfaces expostas em busca de vulnerabilidades, após a aplicação estar em execução.

---

#### **Vantagens da implementação**

* Encontra problemas em tempo de execução que não podem ser identificados pela análise estática;
* Identifica mais rapidamente problemas de autenticação e configuração de servidores;
* Lista as falhas que ficam visíveis apenas quando um usuário de fato efetua um login.

---

### **Ferramentas para DAST**

Ferramenta       | Link para acesso
:---------------:|:----------------:
OWASP ZAP        | [https://owasp.org/www-project-zap/](https://owasp.org/www-project-zap/)
Arachni          | [https://www.arachni-scanner.com/](https://www.arachni-scanner.com/)
SQLmap           | [http://sqlmap.org/](http://sqlmap.org/)
Gauntlt          | [http://gauntlt.org/](http://gauntlt.org)
BDD Security     | [https://github.com/iriusrisk/bdd-security](https://github.com/iriusrisk/bdd-security)
Nikto            | [https://github.com/sullo/nikto](https://github.com/sullo/nikto)
Golismero        | [https://github.com/golismero/golismero](https://github.com/golismero/golismero)

---

![bg right:30%](images/DevSecOps-SAST-Pipeline.png)
Para proteger a camada do **CONTAINER** podemos citar algumas ferramentas interessantes de **image scan** como o **Clair** ou  **anchore-engine**.

---
<style scoped>
{
   font-size: 35px;
}
</style>

![bg left:30% 80%](images/kubesec.jpeg)
Para proteção do **cluster** podemos usar o [**kubesec**](https://github.com/controlplaneio/kubesec) que é uma ferramenta de análise e scanner de segurança **k8s** de código aberto.

Ele analisa seu manifesto yaml atribuindo uma pontuação *(maior pontuação é melhor)* e detalhes sobre os possíveis problemas encontrados.

---
<style scoped>
{
   font-size: 35px;
}
</style>

![bg right:30%](images/DevSecOps-SAST-Pipeline.png)

Para proteger a camada da **cloud** é importante se atentar pelo menos aos pontos:

- IaC Versionada
- Controles de Acesso
- Hardening de instâncias
- Fluxo de acesso à aplicação
- Regras de ﬁrewall
- Alta disponibilidade

---

### **Exemplo de Utilização SAST & DAST**

![w:800](images/jenkins_logo.png)

###### *Jenkinsfile utilizado: [https://gitlab.com/sg-wolfgang/devsecops-campusparty-bsb-2022/-/blob/main/Jenkinsfile](https://gitlab.com/sg-wolfgang/devsecops-campusparty-bsb-2022/-/blob/main/Jenkinsfile)*

---
<style scoped>
pre {
   font-size: 23px;
}
</style>

### **SAST com Horusec**
```yaml
        stage('SAST - Horusec') {
            agent { node 'localhost' }        
            steps {
                sh 'git clone https://github.com/juice-shop/juice-shop juice-shop'
                sh 'horusec start -a ${KEY} \
                -u http://10.182.0.3:8000 -p ${WORKSPACE}/'
            }
        }

```

---

### **Horusec Dashboard**
![w:600](images/horusec_dashboard.jpeg)

---
<style scoped>
pre {
   font-size: 23px;
}
</style>

### **SAST com Sonarqube**

```yaml
stage('SAST com Sonarqube'){
            environment {
                scanner = tool 'sonar-scanner'
            }
            steps{
                withSonarQubeEnv('sonarqube') {
                    sh "${scanner}/bin/sonar-scanner -Dsonar.projectKey=$NAME_APP \
                    -Dsonar.sources=${WORKSPACE}/ \
                    -Dsonar.projectVersion=${BUILD_NUMBER}"
                }
            }
```
---

### **SonarQube Report**

![w:800](images/soanrqube-scanner-results.png)

---
<style scoped>
pre {
   font-size: 23px;
}
</style>

### **DAST com Golismero**

```yaml
stage('DAST com Golismero'){
    environment {
        APP_URL = "http://juiceshop.imsafe.com.br/"
    }
    agent { node 'sentinel' }
    steps{
        sh "golismero scan $APP_URL -o report${BUILD_NUMBER}.html"

        publishHTML([
            allowMissing: false,
            alwaysLinkToLastBuild: true,
            keepAll: true,
            reportDir: '/report-location/',
            reportFiles: 'dast-report-${BUILD_NUMBER}.html',
            reportName: 'DAST REPORT'
        ])
    }
```
---

### **Golismero Report**

![w:700](images/golismero-report.png)

---

![grayscale blur:3px bg left:40%](images/bd-hacker.jpg)
### **Pentest**
O *pentest* tradicional é muito importante no ciclo de desenvolvimento seguro. Esta etapa deverá ser executada por analistas de segurança que estressarão a aplicação ao máximo visando encontrar novas falhas que não foram vistas anteriormente.

---

### **Ferramentas para Pentest**

Ferramenta       | Link para acesso
:---------------:|:----------------:
Kali Linux Tools | [https://www.kali.org/tools/](https://www.kali.org/tools/)

---

#### Dúvidas?

![bg blur:1px left:70%](images/duvida.jpg)

---

## **Obrigado!**

Vamos nos conectar?
* **Site:** [sgoncalves.tec.br](https://sgoncalves.tec.br)
* **E-mail:** [samuel@sgoncalves.tec.br](https://sgoncalves.tec.br/contato)
* **Linkedin:** [linkedin.com/in/samuelgoncalvespereira/](linkedin.com/in/samuelgoncalvespereira/)
* **Telegram:** [t.me/Samuel_gp](t.me/Samuel_gp)
* **Todas as redes:** [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---
### Fontes Bibliográficas

<style scoped>
  p {
  font-size: 19pt;
  list-style-type: circle;
}
</style>
[https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/](https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/)
[https://www.redhat.com/pt-br/topics/devops/what-is-devsecops](https://www.redhat.com/pt-br/topics/devops/what-is-devsecops)
[https://www.ibm.com/br-pt/cloud/learn/devsecops](https://www.ibm.com/br-pt/cloud/learn/devsecops)
[https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/](https://promovesolucoes.com/devsecops-seguranca-continua-lgpd/)
[https://4linux.com.br/cursos/treinamento/devsecops-seguranca-em-infraestrutura-e-desenvolvimento-agil/](https://4linux.com.br/cursos/treinamento/devsecops-seguranca-em-infraestrutura-e-desenvolvimento-agil/)
[https://blog.4linux.com.br/devsecops-implementacao-em-6-passos/](https://blog.4linux.com.br/devsecops-implementacao-em-6-passos/)
[https://blog.gitguardian.com/security-tools-shift-left/](https://blog.gitguardian.com/security-tools-shift-left/)
[https://michelleamesquita.medium.com/entendendo-o-ciclo-de-vida-de-desenvolvimento-de-software-seguro-ssdlc-ccc173f583de](https://michelleamesquita.medium.com/entendendo-o-ciclo-de-vida-de-desenvolvimento-de-software-seguro-ssdlc-ccc173f583de)